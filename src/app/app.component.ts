import { Component } from '@angular/core';

export interface Items {
  name: string;
  uid: string;
  id: number;
}

const sampleData: Items[] = [
  { name: 'Chirag Rawal', uid: '17BCS4132', id: 1 },
  { name: 'Risav Chaudhary', uid: '17BCS4129', id: 2 },
  { name: 'Kameshwar Sahoo', uid: '17BCS4121', id: 3 },
  { name: 'Kartik Chaturvedi', uid: '17BCS4116', id: 4 },
  { name: 'Risav Chaudhary', uid: '17BCS4129', id: 5 },
  { name: 'Kameshwar Sahoo', uid: '17BCS4121', id: 6 },
  { name: 'Kartik Chaturvedi', uid: '17BCS4116', id: 7 },
  { name: 'Risav Chaudhary', uid: '17BCS4129', id: 8 },
  { name: 'Kameshwar Sahoo', uid: '17BCS4121', id: 9 },
  { name: 'Kartik Chaturvedi', uid: '17BCS4116', id: 10 },
  { name: 'Risav Chaudhary', uid: '17BCS4129', id: 11 },
  { name: 'Kameshwar Sahoo', uid: '17BCS4121', id: 12 },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  displayedColumns: string[] = ['id', 'uid', 'name'];
  dataSource = sampleData;
  title = 'LabMST4132';
}
